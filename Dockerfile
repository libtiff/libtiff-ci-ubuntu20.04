# syntax = docker/dockerfile:1.0-experimental
FROM ubuntu:20.04
MAINTAINER tiff@lists.osgeo.org

# Install required build dependencies
RUN apt update \
  && DEBIAN_FRONTEND=noninteractive apt install -y -qq --no-install-recommends \
    autoconf \
    automake \
    build-essential \
    cmake \
    curl \
    ca-certificates \
    doxygen \
    libtool \
    git \
    graphviz \
    locales \
    ninja-build \
    zip \
    unzip \
    wget \
    libgtest-dev \
    libjpeg8-dev \
    libjbig-dev \
    liblzma-dev \
    zlib1g-dev \
    libdeflate-dev \
    libzstd-dev \
    libwebp-dev \
  && locale-gen en_US.UTF-8

COPY install-lerc.sh /tmp
RUN cd /tmp \
  && sh install-lerc.sh \
  && rm install-lerc.sh

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
