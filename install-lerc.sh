#!/bin/sh

set -e
set -x

if [ -z "${LERC_VER}" ]
then
    LERC_VER=2.2.1
fi

# Build lerc
wget https://github.com/Esri/lerc/archive/v${LERC_VER}.tar.gz
tar xvzf v${LERC_VER}.tar.gz
mkdir lerc-${LERC_VER}/_build
cd lerc-${LERC_VER}/_build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
cmake --build . --target install
cd ../..
rm -rf lerc-${LERC_VER}
ldconfig

